# Simple rc file that can be sourced e.g. in `~/.bash_profile` or `~/.zshrc`.
# It contains several helpful aliases and functions for the daily use.

# Resolve FQPN of an executable from $PATH. Especially helpful with Nix.
function rsl() {
  fp="$($(command -v which) "$1")"
  [ -L "$fp" ] && { readlink "$fp"; return 0; }
  [ -f "$fp" ] && { echo "$fp"; return 0; }
  type "$1"
}

# `git log` for a Nix-package
function pl() {
  filename="$(EDITOR=echo nix edit -f . "$1")"
  [ -n "$filename" ] && git log "$filename"
}

# Simple wrapper for SSH which properly configures the env first.
function ssh() {
  LANG="en_US.UTF-8" \
  LC_ALL="$LANG" \
  TERM="xterm-color" \
  /run/current-system/sw/bin/ssh "$@"
}

# Use `exa` instead of the standard `ls` as it provides a way better interface
alias ls="exa"
alias ll="exa -lh --group-directories-first --git --group"
alias l="exa -lah --group-directories-first --git --group"
alias tree="exa -T"

# Some networking related shortcuts
alias my-ip="curl http://ping.lrz.de/cgi-bin/ip.cgi"
alias dig6="dig -t AAAA"

# Several nix-related tools for shortcuts and easier local builds
alias build='nix build -f . -L'
alias build-local="build --builders ''"
alias build-offline="build-local --substituters ''"

alias b='build'
alias bl='build-local'
alias br='build-remote'
alias bo='build-offline'

alias nixos-release-build="nix build -L -f nixos/release.nix --arg supportedSystems '[ builtins.currentSystem ]' --builders ''"
alias pe='nix edit -f .'
alias nxvm='NIX_PATH=nixpkgs=$(pwd) nixos-build-vms'

alias flup='nix flake lock --impure --recreate-lock-file --commit-lock-file'

# Some shortcuts for file-search
alias ff='fd -t f'
alias fda='fd --no-ignore-vcs --no-ignore -H -L'
alias fd='fd -t d'
alias fe='fd -t f -e'

# General helpers
alias diff="colordiff"
alias ip="ip -c"
alias t="tail -f"
alias grep="grep --color"
alias dcm="docker-compose"
alias http="curlie"

# Pager settings
export LESS="-r --quit-if-one-screen"
export BAT_PAGER='less -R'
export PAGER='less -R'
