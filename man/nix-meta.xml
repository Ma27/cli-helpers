<refentry xmlns="http://docbook.org/ns/docbook"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:xi="http://www.w3.org/2001/XInclude"
      version="5.0"
      xml:id="sec-nix-meta">

 <info>
  <author><personname><firstname>Maximilian</firstname><surname>Bosch</surname></personname>
   <contrib>Author</contrib>
  </author>
  <copyright><year>2020</year><holder>Maximilian Bosch</holder></copyright>
 </info>

 <refmeta>
  <refentrytitle>nix-meta</refentrytitle>
  <manvolnum>1</manvolnum>
  <refmiscinfo class="source">cli-helpers</refmiscinfo>
  <refmiscinfo class="version"><xi:include href="../version.txt" parse="text"/></refmiscinfo>
 </refmeta>

 <refnamediv>
  <refname>nix-meta</refname>
  <refpurpose>
   Query Nix <literal>meta</literal> expressions <citerefentry>
   <refentrytitle>jq</refentrytitle><manvolnum>1</manvolnum>
   </citerefentry>.
  </refpurpose>
 </refnamediv>

 <refsynopsisdiv>
  <cmdsynopsis>
   <command>nix-meta</command>
   <arg choice='opt'>
    <group>
     <arg choice='plain'><option>--path</option></arg>
     <arg choice='plain'><option>-p</option></arg>
    </group>
    <replaceable>file</replaceable>
   </arg>
   <arg choice='opt'>
    <option>-A</option>
    <replaceable>attr</replaceable>
   </arg>
  </cmdsynopsis>
 </refsynopsisdiv>

 <refsection>
  <title>Description</title>
  <para>
   Queries <literal>meta</literal>-information from Nix derivations as JSON
   using <citerefentry><refentrytitle>jq</refentrytitle><manvolnum>1</manvolnum>
   </citerefentry>.
  </para>
 </refsection>

 <refsection>
  <title>Options</title>
  <variablelist>
   <varlistentry>
    <term>
     <option>-p</option> / <option>--path</option>
    </term>
    <listitem>
     <para>
      Path to the Nix expression to evaluate. By default this is <literal>&lt;nixpkgs&gt;</literal>.
      If a <filename>default.nix</filename> exists in <envar>PWD</envar>, this expression is used
      instead by default.
     </para>
     <para>
      If <replaceable>file</replaceable> is an entry of <envar>NIX_PATH</envar>, the path
      from <envar>NIX_PATH</envar> is used instead.
     </para>
    </listitem>
   </varlistentry>
   <varlistentry>
    <term><option>-A</option></term>
    <listitem>
     <para>Only display a sub-attribute of the output.</para>
    </listitem>
   </varlistentry>
  </variablelist>
 </refsection>

 <refsection>
  <title>Examples</title>
  <para>
   Only show list of <literal>maintainers</literal> from <literal>meta</literal>:
   <screen>
    <prompt>$ </prompt> nix-meta hello -A maintainers
   </screen>
  </para>
  <para>
   Modify the output with <citerefentry><refentrytitle>jq</refentrytitle><manvolnum>1</manvolnum>
   </citerefentry>:
   <screen>
    <prompt>$ </prompt> nix-meta hello -A maintainers | jq 'map(.email)'
   </screen>
  </para>
 </refsection>

</refentry>
