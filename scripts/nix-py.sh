#! /usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}
check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }
  echo "$dep"
}
print_help() {
  exec man nix-py
}

target="python3"
evalfile="<nixpkgs>"
pkgs=()

while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      print_help
      exit 0
      ;;
    -t)
      shift
      target="$1"
      ;;
    -f)
      shift
      evalfile="$1"
      [[ "$evalfile" =~ ^(\.) ]] && evalfile="./$evalfile"
      ;;
    *)
      pkgs+=("$1")
      ;;
  esac
  shift
done

[ "${#pkgs}" -eq 0 ] && { print_help; exit 1; }

expr="
with import $evalfile { };
$target.withPackages (ps: with ps; [ ${pkgs[*]} ])
"
set -e
build="$(check_dep nix-build)"
set +e

outp="$("$build" -E "$expr")"
[ -d "$outp" ] || error "Build failed, aborting..."

exec "$outp/bin/python"
