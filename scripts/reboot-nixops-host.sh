#!/usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }

  echo "$dep"
}

show_help() {
  exec man reboot-nixops-host
}

await_ssh() {
  until nc -z "$1" 22 -w1 2>/dev/null; do
    echo -e "\e[33m$2\e[0m"
    sleep 2
  done
}

set -e
nixops_exec=$(check_dep nixops)
ssh_exec=$(check_dep ssh)
pass_exec=$(check_dep pass)
set +e

deployment=
machines=()
pass_paths=()
pass_entry_default="nixops/default-luks"
dropbear_pubkey=
unlock_only=0

while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      show_help
      exit 0
      ;;
    --pubkey)
      shift
      if [ -n "$dropbear_pubkey" ]; then
        error "Cannot set dropbear pubkey multiple times"
      fi
      dropbear_pubkey="$1"
      ;;
    --unlock-only|-u)
      unlock_only=1
      ;;
    --deployment|-d)
      if [ -n "$deployment" ]; then
        error "Cannot set multiple deployments atm!"
      fi
      shift

      deployment="$1"
      "$nixops_exec" info -d "$deployment" 2>/dev/null

      if [ $? -ne 0 ]; then
        error "Deployment $deployment can't be found by nixops!"
      fi
      ;;
    *)
      machines+=("$1")
      if [[ "$2" =~ ^(-p|--pass-path)$ ]]; then
        shift 2

        pass_paths+=("$1")
      else
        pass_paths+=("$pass_entry_default")
      fi
      ;;
  esac

  shift
done

if [ "${#machines}" = "0" ]; then
  show_help
  exit 1
fi

if [ -z "$dropbear_pubkey" ]; then
  error "Dropbear-pubkey needs to be set!"
fi
if [ ! -f "$dropbear_pubkey" ]; then
  error "Dropbear pubkey $dropbear_pubkey doesn't appear to be a valid file!"
fi

for index in "${!machines[@]}"; do
  i="${machines[$index]}"
  path="${pass_paths[$index]}"
  mtype=$("$nixops_exec" show-option "$i" deployment.targetEnv 2>/dev/null)

  if [ $? -ne 0 ]; then
    error "Trying to retrieve deployment env for $i failed! Are you sure the machine exists (Deployment ${deployment:-default} is used!)"
  fi

  if [[ "$(echo "$mtype" | xargs echo)" != "none" ]]; then
    error "Currently only servers with the \`none' backend are supported!"
  fi

  targetIp=$("$nixops_exec" show-option "$i" deployment.targetHost | xargs echo)

  if [ "$unlock_only" -eq "0" ]; then
    echo -e "\e[1mRebooting $i in 3 seconds...\e[0m"
    sleep 3

    "$ssh_exec" "root@$targetIp" "reboot" 2>/dev/null

    #ensure that the service stopped and started again...
    until ! nc -z "$targetIp" 22 -w1 2>/dev/null; do
      echo -e "\e[33mWaiting for $targetIp to shut down\e[0m"
      sleep 2
    done
  else
    echo -e "\e[1mWaiting for $i to become available...\e[0m"
  fi

  await_ssh "$targetIp" "Waiting for $targetIp to start initrd host"

  echo -e "\e[33mAutomatically typing LUKS passphrase\e[0m"

  # it seems as `-o UserKnownHostsFile` doesn't like `<()`-constructed files.
  tmp_known_hosts=$(mktemp)
  echo "$targetIp $(cat "$dropbear_pubkey")" > "$tmp_known_hosts"

  out=$("$ssh_exec" "root@$targetIp" \
    -o UserKnownHostsFile="$tmp_known_hosts" \
    "printf %s '$("$pass_exec" show "$path" | sed "s/'/'\"'\"'/g")' | cryptsetup-askpass" 2>/dev/null | tee /dev/tty)

  if ! echo "$out" | grep -q "Waiting 10 seconds for LUKS"; then
    rm "$tmp_known_hosts"
    if [ "$unlock_only" -eq 0 ]; then
      error "Failed to set luks key! Please check your logs!"
    else
      error "Failed to set luks key! Is the server actually rebooted? (You attempt to unlock a server without reboot with \`-u' being set)"
    fi
  fi

  rm "$tmp_known_hosts"
  echo -e "\n\e[32mSuccessfully rebooted $i ($targetIp)\e[0m"

  echo -e "\e[1mSending deployment keys to server\e[0m"
  await_ssh "$targetIp" "Waiting for sshd to become available to upload secrets from nixops"
  "$nixops_exec" send-keys --include "$i"

  if [ -n "$(command -v query-nixops-machines 2>/dev/null)" ]; then
    containers="$($(command -v query-nixops-machines) container -n "$i" 2>/dev/null)"
    if [ -n "$containers" ]; then
      echo "$containers" | xargs -I % "$nixops_exec" send-keys --include %
    fi
  fi
done
