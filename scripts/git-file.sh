#!/usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-"Unknown error!"}\e[0m"
  exit 1
}


file=${2:-}
revision=${1:-}

if [ "$file" = "--help" ] || [ "$revision" = "--help" ] || [ -z "$file" ]; then
  exec man git-file
fi

git_exec=$(command -v git)
[ $? -eq 0 ] || error "Git is not installed or not in \$PATH!"

{ $git_exec rev-parse --git-dir 2>&1; } >/dev/null || error "Not in a git repository"

"$git_exec" rev-parse --verify "$revision" >/dev/null || error "Revision $revision doesn't exist!"

vim_exec=$(command -v vim)
[ $? -eq 0 ] || error "\`vim\` is not installed or not in \$PATH!"

{ "$git_exec" ls-tree -r "$revision" --name-only | grep "^$file"; } || \
  error "File $file appears to not exist on $revision!"

case "$(basename "$file")" in
  (?*.*) ext="${file##*.}";;
  (*) ext=;;
esac

"$vim_exec" -R <("$git_exec" show "$revision:$file") -c 'set nu' ${ext:+-c "set ft=$ext"}
