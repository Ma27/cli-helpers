#!/usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }

  echo "$dep"
}

show_help() {
  exec man borg-nixops
}

machine=
args=()

while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      show_help
      exit 0
      ;;
    -m)
      shift
      machine="$1"
      ;;
    *)
      args+=("$1")
  esac
  shift
done

if [ -z "$machine" ]; then
  show_help
  exit 1
fi

set -e
nixops_exec=$(check_dep nixops)
borg_exec=$(check_dep borg)
set +e

remote=$("$nixops_exec" show-option "$machine" ma27.backups.remote 2>/dev/null)
if [ $? -ne 0 ]; then
  error "Machine $machine doesn't appear to exist in deployment ${NIXOPS_DEPLOYMENT:-default deployment}!"
  exit 1
fi

BORG_REPO="$(echo "$remote" | xargs echo):$machine"

# only extract without a target -> extract latest archive
if [ "${args[*]}" = "extract" ]; then
  args+=("$BORG_REPO::$(BORG_REPO="$BORG_REPO" "$borg_exec" list --last 1 | awk '{ print $1 }')")
fi

BORG_REPO="$BORG_REPO" "$borg_exec" "${args[@]}"
