#!/usr/bin/env bash

# TODO add support to automatically create a fork on github (e.g. with hub)?

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }

  echo "$dep"
}

print_help() {
  exec man clone-fork
}

github_handle="Ma27" # sensitive default :p
project_dir="$HOME/Projects"
repos=()
while [ $# -gt 0 ]; do
  case "$1" in
    --username)
      github_handle="$1"
      ;;
    --dir)
      shift
      project_dir="$1"
      ;;
    -h|--help)
      print_help
      exit 0
      ;;
    *)
      repos+=("$1")
      ;;
  esac
  shift
done

if [ "${#repos}" -eq 0 ]; then
  print_help
  exit 1
fi

set -e
git_exec=$(check_dep git)
set +e

# Validity of `$project_dir` is checked before.
# shellcheck disable=SC2164
pushd "$project_dir" >&2 2>/dev/null
for i in "${repos[@]}"; do
  target_dir="$(basename "$i")"
  if [ -d "$target_dir" ]; then # TODO consider creating an alternative name
    error "Target directory $target_dir already exists, cannot clone!"
  fi
  "$git_exec" clone git@github.com:"$github_handle"/"$target_dir"

  # shellcheck disable=SC2164
  pushd "$target_dir" >&2 2>/dev/null
  "$git_exec" remote add upstream "git@github.com:$i"

  # shellcheck disable=SC2164
  popd >&2 2>/dev/null
done

# shellcheck disable=SC2164
popd >&1 2>/dev/null
