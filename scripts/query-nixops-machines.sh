#!/usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }

  echo "$dep"
}

show_help() {
  exec man query-nixops-machines
}

deployment=()
node_type=
on_node=

while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      show_help
      exit 0
      ;;
    -d|--deployment)
      shift
      if [ "${#deployment[@]}" -gt 0 ]; then
        error "Only one deployment can be specified for the query!"
      fi
      deployment+=("-d" "$1")
      ;;
    -n|--node)
      shift
      if [ -n "$on_node" ]; then
        error "Target node for a machine can only be specified once!"
      fi
      on_node="$1"
      ;;
    *)
      if [ -n "$node_type" ]; then
        error "Only one target type can be specified for the query!"
      fi
      node_type="${1/host/none}"
      ;;
  esac
  shift
done

if [ -n "$on_node" ] && [ "$node_type" != "container" ]; then
  error "The -n option can only be set when querying containers to match containers to 'deployment.container.host'!"
fi

set -e
nixops_exec=$(check_dep nixops)
sed_exec=$(check_dep sed)
awk_exec=$(check_dep awk)
set +e

# shellcheck disable=SC2016
matches=$("$nixops_exec" info "${deployment[@]}" 2>/dev/null \
  | "$sed_exec" -e '0,/^ \| Name/d; $ d' \
  | "$sed_exec" -e "1 d" \
  | "$awk_exec" -F "|" \
      -v "compare=$node_type" -v "comp_len=${#node_type}" \
      '
        function strip_w(str) { gsub(/^ /,"",str); gsub(/[ ]+$/,"",str); return str; }
        { $4=strip_w($4); $2=strip_w($2);
          if ($4 == compare || comp_len == "0") { print $2; };
        }
       ' \
  )

IFS=$'\n'
results=0
for i in $matches; do
  if [ -n "$on_node" ]; then
    m_name="$("$nixops_exec" show-option "${deployment[@]}" "$i" \
      "deployment.container.host" 2>/dev/null | xargs echo)"
    if [ "$m_name" != "__machine-$on_node" ]; then
      continue
    fi
  fi
  echo "$i"
  results=1
done

if [ -z "$matches" ] || [ $results -eq 0 ]; then
  error "No matches found for type $node_type ${on_node:+"(on host $on_node)"}"
fi
