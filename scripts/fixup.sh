#! /usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }
  echo "$dep"
}

if [[ "$*" =~ (-h|--help) ]]; then
  exec man fixup
fi

value=
if [[ "$*" =~ (-n) ]]; then
  # XXX add better argparse?
  value="$(sed -re "s,^.*-n ([0-9]+) .*$,\1," -e "s,[^0-9],,g" <<< "$*")"
fi

set -e
git_exec=$(check_dep git)
set +e
{ "$git_exec" rev-parse --show-toplevel 2>&1; } >/dev/null || error "Not a git repository!"

current=$("$git_exec" log --oneline -n 1 --format=%h 2>/dev/null)
cmsg=$("$git_exec" log --oneline -n 1 --format=%s 2>/dev/null)

{ "$git_exec" log 2>&1; } >/dev/null || error "No commits yet!"

resume=0
if [ -z "$value" ]; then
  [ -n "$("$git_exec" diff --name-only --cached)" ] || error "No staged changes!"

  "$git_exec" log --oneline --format=%s -n 1 | grep -q '^fixup!' && \
    error "Last commit is about to get rebased, cannot use."

  [[ "$("$git_exec" show | sed -n 2p)" =~ ^(Merge) ]] && \
    error "Last commit is a merge commit!"

  echo -e "\e[32mSquashing staged changes back to ($cmsg/$current)...\e[0m"

  { "$git_exec" commit --fixup="$current" 2>&1; } >/dev/null
  { "$git_exec" stash save "temporary stash while fixing up" | grep -v "^No local" 2>&1; resume=$?; } >/dev/null
elif ! "$git_exec" diff-index --quiet HEAD; then
  error "Cannot fixup commits on a dirty working tree!"
elif ! { "$git_exec" show HEAD~"$value" 2>&1; } >/dev/null; then
  error "Cannot fixup $value commit(s)!"
elif "$git_exec" log HEAD...HEAD~"$value" 2>&1 | grep -q "^Merge:"; then
  error "Within HEAD...HEAD~$value are merge commits!"
fi

edit=:
n=2

[ -n "$value" ] && { edit="sed -i -e '2,\$s/^pick/fixup/g'"; n="$value"; }

GIT_SEQUENCE_EDITOR=$edit "$git_exec" rebase HEAD~"$n" --autosquash -i
[ -z "$value" ] && { [ "$resume" -eq 1 ] || "$git_exec" stash pop 2>&1; } >/dev/null

echo -e "\e[32mFixed staged changes up back into $("$git_exec" log --oneline -n 1 --format=%h)\e[0m"
