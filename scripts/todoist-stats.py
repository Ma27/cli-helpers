#! /usr/bin/env python3

from datetime import datetime, timezone
from multiprocessing import Process
from os import getenv, execvp
from sys import exit, argv, stderr
import json
import todoist
import pytz
import tzlocal

NOW = datetime.now()
FORMAT = '%Y-%m-%d'
FORMAT_W_TIME = f"{FORMAT}T%H:%M:%S"
TODAY = NOW.strftime(FORMAT)
TZ = str(tzlocal.get_localzone())


def is_today(date):
    return date.date() == datetime.now(tz=pytz.timezone(TZ)).date()


# We only want to show due and completed items.
# Those are tasks that match the following criteria:
# * Items that are not done yet (those are counted separately)
# * Items that are due {,before} today
# * Uncompleted items without due date are ignored
def is_item_due(obj):
    if obj['date_completed'] is not None:
        return False

    if obj['due'] is None or obj['due']['date'] is None:
        return False

    fmt = FORMAT if 'T' not in obj['due']['date'] else FORMAT_W_TIME
    return datetime.strptime(obj['due']['date'], fmt) <= NOW


def normalize_time(time_str):
    return datetime.strptime(time_str, '%Y-%m-%dT%H:%M:%SZ').replace(
        tzinfo=timezone.utc
        ).astimezone(tz=None)


def main():
    if len(argv) >= 2 and argv[1] in ['-h', '--help']:
        execvp('man', ['man', 'todoist-stats'])

    token = getenv('TODOIST_TOKEN')
    if token is None:
        print('Please assign your Todoist API key to TODOIST_TOKEN!', file=stderr)
        exit(1)

    client = todoist.TodoistAPI(token)

    if '--sync' in argv:
        client.sync()
    else:
        # Simply abort if `sync()` takes too long, e.g. due to missing network.
        # `python-todoist` already does caching, so after at most 2 seconds we
        # simply fall back to locally cached data.
        proc = Process(target=client.sync)
        proc.start()
        proc.join(timeout=2)
        proc.terminate()

    tempdir = getenv('TEMP')
    if tempdir is None:
        tempdir = '/tmp'
    filename = f"{tempdir}/todoist_completed_vals"

    try:
        completed = client.completed.get_all()['items']
    except Exception:
        with open(filename, "r") as f:
            completed = json.loads(f.read())

    done_today = len([a for a in completed if is_today(normalize_time(a['completed_date']))])

    due = [a for a in client.items.all() if is_item_due(a)]
    tbd_today = len(due) + done_today

    with open(filename, "w+") as f:
        f.write(json.dumps(completed))

    print("  {0:d}/{1:d}".format(done_today, tbd_today))


if __name__ == '__main__':
    main()
