#!/usr/bin/env bash

log() {
  echo >&2 -e "\e[32m$1\e[0m"
}

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }

  echo "$dep"
}

print_help() {
  exec man build-remote
}

host=
arch=
exprs=()
download=0

while [ $# -gt 0 ]; do
  case "$1" in
    -H|--host) shift; host="$1";;
    -a|--arch) shift; arch="$1";;
    -A|-E) exprs+=("$1" "$2"); shift;;
    -h|--help) print_help; exit 0;;
    -d) download=1;;
    *) print_help; exit 1;;
  esac
  shift
done

[ -n "$host" ] || error "The host option (--host hostname) must not be empty!"

set -e
nix="$(check_dep nix)"
instantiate="$(check_dep nix-instantiate)"
jq="$(check_dep jq)"
set +e

download() {
  mapfile -t outpaths < <("$nix" show-derivation "${@}" \
    | "$jq" -r '.[].outputs|map(.path)|join("\n")')

  if [[ "$download" -eq 1 ]]; then
    log "Downloading ${#outpaths} paths including closure from ${host}:";
    "$nix" copy --from ssh://"$host" "${outpaths[@]}" || exit $?;
  else
    log "Nothing to download (specify -d for download). The following paths were built remotely:"
  fi

  local IFS=$'\n'
  echo "${outpaths[*]}"
}

input="$("$instantiate" "${exprs[@]}" ${arch:+--argstr system $arch} | sed -e 's/!.*$//g')" \
  || exit $?

mapfile -t paths <<< "$input"

log "Copying ${#paths} path(s) including closure to ${host}..."
"$nix" copy --derivation "${paths[@]}" --to ssh://"$host" || exit $?
if [ -n "$(nix build --dry-run "${paths[@]}" --store ssh-ng://"$host" 2>&1)" ]; then
  log "Building..."
  "$nix" build -L "${paths[@]}" --keep-going --store ssh-ng://"$host" || exit $?
fi

download "${paths[@]}"
