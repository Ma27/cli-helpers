#!/usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }

  echo "$dep"
}

print_help() {
  exec man nixos-generations
}

with_drv_file=0
with_c=1
only_drvs=0
latest=0
while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      print_help
      exit 0
      ;;
    -d|--with-drv)
      with_drv_file=1
      ;;
    -c|--no-c)
      with_c=0
      ;;
    -o|--only-drvs)
      only_drvs=1
      ;;
    -l|--latest)
      latest=1
      ;;
    *)
      print_help
      exit 1
  esac
  shift
done

find="$(check_dep find)"

# TODO also support local profiles
vals=$("$find" '/nix/var/nix/profiles/' -maxdepth 1 -name 'system-*' | sort)

[ "$latest" -eq 1 ] && vals="$(tail -1 <<< "$vals")"

for i in $vals; do
  num="$(basename "$i" | awk -F- '{ print $2 }')"
  mod_date="$(date -d "$(stat -c %y "$i")" +'%Y.%m.%d-%H:%M')"
  cur=
  [ "$(readlink '/nix/var/nix/profiles/system')" = "$(basename "$i")" ] && [ "$with_c" -eq 1 ] \
    && cur="   (current)"

  store_path=""
  if [ "$only_drvs" -eq 1 ]; then
    nix_store="$(check_dep nix-store)" || exit $?
    "$nix_store" --query --deriver "$(readlink "$i")"
  else
    nix_store="$(check_dep nix-store)" || exit $?
    [ "$with_drv_file" = 1 ] && store_path=" $("$nix_store" --query --deriver "$(readlink "$i")")"

    echo "$num $mod_date$store_path$cur"
  fi
done
