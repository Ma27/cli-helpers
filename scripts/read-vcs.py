#!/usr/bin/env python3

from argparse import ArgumentParser
from datetime import datetime
from icalendar import Calendar
from logging import warning
from os import path
from rich.console import Console
from textwrap import dedent


def parse_mail(ical_val):
    pref = 'mailto:'
    ret = ical_val
    if type(ical_val) != str:
        ret = str(ical_val.to_ical(), 'utf-8')
    if ret.lower().startswith(pref):
        return ret[len(pref):]

    return ret


def to_date(ical_val):
    return datetime.strptime(str(ical_val.to_ical(), 'utf-8'), '%Y%m%dT%H%M%S')


def attendees(event):
    rs = event.get('ATTENDEE')
    if type(rs) == list:
        return rs
    return [rs]


def mk_ev(event):
    people = [{
        'email': parse_mail(event.get('ORGANIZER')),
        'organizer': True,
    }]
    people.extend([{'email': parse_mail(x), 'organizer': False} for x in attendees(event)])

    return {
        'start': to_date(event.get('DTSTART')),
        'end': to_date(event.get('DTEND')),
        'summary': str(event.get('SUMMARY').to_ical(), 'utf-8'),
        'people': people,
        'description': event.get('DESCRIPTION'),
    }


def extract_data(vcard):
    return [mk_ev(e) for e in vcard.walk('VEVENT')]


def display_attendee(person):
    rs = f"{person['email']}"
    if person['organizer']:
        rs += " [italic](Organizer)[/italic]"

    return rs


def display_event(event):
    fullfmt = '%Y-%m-%d %H:%M'
    dfmt, hfmt = fullfmt.split(' ')

    start = event['start']
    end = event['end']
    if start.date() == end.date():
        info = " ".join([
            f"At [bold]{start.strftime('%A')}[/bold], [italic]{start.strftime(dfmt)}[/italic]",
            f" from [italic]{start.strftime(hfmt)}[/italic] until",
            f"[italic]{end.strftime(hfmt)}[/italic]"
        ])
    else:
        info = " ".join([
            f"From [italic]{start.strftime(fullfmt)}[/italic] until",
            f"[italic]{end.strftime(fullfmt)}[/italic]"
        ])

    attendees = "\n".join([
        f"* {display_attendee(attendee)}" for attendee in event['people']
    ])

    return "\n".join(map(dedent, f"""
        [bold]{event['summary']}[/bold]

        {info}

        Attendees:
        {attendees}
    """.split("\n"))).strip()


def display_events(events, n):
    return [display_event(ev) for ev in events[:n]]


def main():
    console = Console(force_terminal=True)

    argp = ArgumentParser(description='Extract information from `.vcs`-files.')
    argp.add_argument('file', help='.vcs file to display')
    argp.add_argument('-n', '--num-events', default=1, type=int, help='Amount of events to display')
    args = argp.parse_args()

    if not path.exists(args.file):
        console.print(f"Error: {args.file} doesn't exist or is not readable!")
        exit(1)

    with open(args.file, 'rb') as file:
        console.print("\n".join(
            display_events(extract_data(Calendar.from_ical(file.read())), args.num_events)
        ))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        warning("Aborting...")
