#!/usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }

  echo "$dep"
}

if [ -f "$(pwd)/default.nix" ]; then
  path="$(pwd)/default.nix"
else
  path="<nixpkgs>"
fi

attr_path=
argpath=

while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      exec man nix-meta
      ;;
    --path|-p)
      shift
      if [[ "$1" =~ ^(<.*>)$ ]]; then
        path="$1"
        [[ "$NIX_PATH" == *$(echo "$1" | sed -r 's,<(.+)>$,\1,')=* ]] \
          || error "Cannot find $path literal in NIX_PATH!"
      else
        path="$(realpath "$1")"
        [[ -e "$path" ]] || error "Expression path $1 doesn't exist!"
      fi
      ;;
    -A)
      shift
      argpath="$1"
      ;;
    *)
      [ -n "$attr_path" ] && error "Only one attribute path can be set!"
      attr_path="$1"
      ;;
  esac

  shift
done

set -e
nix_inst_exec=$(check_dep nix-instantiate)
jq_exec=$(check_dep jq)
set +e

evalCode="
let
  applyIfFn = fn: if builtins.isFunction fn then fn {} else fn;
  importPath = applyIfFn (import $path);
  lib = import <nixpkgs/lib>;
  pkgArg = if \"$attr_path\" == \"\"
    then importPath
    else lib.attrByPath (lib.splitString \".\" \"$attr_path\") null importPath;
in
  if pkgArg == null then throw \"No value under attr path $attr_path!\"
  else if !(lib.isDerivation pkgArg) then throw \"The current package doesn't appear to be a derivation!\"
  else if !(pkgArg ? meta) then throw \"No meta section given for attr path!\"
  else pkgArg.meta
"

"$nix_inst_exec" -E "$evalCode" --json --strict --eval | "$jq_exec" ${argpath:+.$argpath}
