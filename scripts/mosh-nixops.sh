#! /usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }
  echo "$dep"
}

if [[ "${1:-}" =~ ^(-h|--help)$ ]]; then
  exec man mosh-nixops
fi

set -e
mosh=$(check_dep mosh)
nixops=$(check_dep nixops)
set +e
host="${1:-}"
if [ -z "$host" ]; then
  error "Please declare the host you'd like to connect to (or set -h for further reference)."
fi

env=$("$nixops" show-option ${NIXOPS_DEPLOYMENT:+-d $NIXOPS_DEPLOYMENT} "$host" deployment.targetEnv 2>/dev/null | xargs echo)

if [ "$env" = "container" ]; then
  ip=$("$nixops" show-option ${NIXOPS_DEPLOYMENT:+-d $NIXOPS_DEPLOYMENT} "$host" deployment.container.localAddress 2>/dev/null | xargs echo)
else
  ip=$("$nixops" show-option ${NIXOPS_DEPLOYMENT:+-d $NIXOPS_DEPLOYMENT} "$host" deployment.targetHost 2>/dev/null | xargs echo)
fi

[ -n "$ip" ] || error "IP empty, are you sure that $host exists and provides a deployment.targetHost field?"

exec "$mosh" "${2:-root}@$ip"
