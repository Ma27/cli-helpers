#! /usr/bin/env bash

error() {
  echo >&2 -e "\e[31m${1:-Unknown error}\e[0m"
  exit 1
}

# Evil hack to get current dir silently without git (for purity)
# to access 3rdparty files. This env-var should be set in a wrapper when
# packaging this script!
THIRDPARTY_DIR="${THIRDPARTY_DIR:-"$(pwd)/3rdparty"}"
[ ! -d "$THIRDPARTY_DIR" ] && \
  error "The env-var THIRDPARTY_DIR is missing! It's recommended to install \
this script via Nix to fix this. Another option would be to set THIRDPARTY_DIR to <cli-helpers/3rdparty>."

check_dep() {
  dep="$(command -v "${1}" 2>/dev/null)"
  [ $? -eq 0 ] || { error "Please ensure that ${1} is installed in your PATH!"; exit 1; }
  echo "$dep"
}

print_help() {
  exec man nix-flame
}

open_file=0
target_file=
nix_args=()
eval_file=
ret=0

# If invoked with no args, this would evaluate `<nixpkgs>` and save it as
# a temporary file. This is pretty useless as `nix-instantiate '<nixpkgs>'` always fails
# and some users probably invoke a cmd without any args to check the usage.
[ -z "$*" ] && { print_help; exit 1; }

while [ $# -gt 0 ]; do
  case "$1" in
    -h|--help)
      print_help
      exit 0
      ;;
    -o|--open)
      [ -z "$BROWSER" ] && error "No BROWSER var set. Don't know how to open the final SVG."
      open_file=1
      ;;
    -s|--save)
      shift
      target_file="$1"
      ;;
    --parse|--eval)
      nix_args+=("$1")
      ;;
    -A|-E)
      nix_args+=("$1" "$2")
      shift
      ;;
    *)
      [ -n "$eval_file" ] && error "Currently only one file to evaluate is possible!"
      eval_file="$1"
  esac
  shift
done

set -e
nixinst="$(check_dep nix-instantiate)"
flamegraph="$(check_dep flamegraph.pl)"
set +e

[ -z "$eval_file" ] && eval_file='<nixpkgs>'
[ -z "$target_file" ] && target_file="$(mktemp "${TMP:-/tmp}/function-trace.XXXXXXXXX.svg")"

# FIXME with `script -qec` we can preserve colors, but need to remove those at
# the pipe for `stack-collapse.py`.
output="$("$nixinst" --trace-function-calls "$eval_file" "${nix_args[@]}" -vvvv --show-trace 2>&1)"
if [ $? -ne 0 ]; then
  ret=1
  echo "$output" | grep -Pzo '.*error(.*\n)*'
fi

echo "$output" \
  | "$THIRDPARTY_DIR/nix/stack-collapse.py" \
  | "$flamegraph" > "$target_file"

[ "$open_file" -eq 1 ] && "$BROWSER" "$target_file"
echo -e "\e[32mSuccessfully saved flamegraph under $target_file\e[0m"

exit $ret
