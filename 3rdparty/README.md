# 3rdparty code

This subtree contains third-party code which was included in this
repository.

## Nix

Licensed under [LGPL](https://github.com/NixOS/nix/blob/c84c843e3360459cf2c993919fb9f89474a46f38/COPYING).
The `COPYING` file is incorporated in the subtree `nix/`.

Please note that this directory only contains `stack-collapse.py` from `<nix/contrib/stack-collapse.py>`
for `scripts/nix-flame.sh`.

## `nix-bash-completions`

Licensed under [BSD3-clause](https://github.com/hedning/nix-bash-completions/blob/207d034520766eae9c8d9d0d3473f09d9b4e2fc2/LICENSE).
The `LICENSE` file is incorporated in the subtree `nix-bash-completions/`.

This subtree contains a patched version which only contains the utility functions for attribute-completion
for scripts that work with Nix expressions.
