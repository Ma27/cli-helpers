# cli-helpers

[![pipeline status](https://gitlab.com/Ma27/cli-helpers/badges/master/pipeline.svg)](https://gitlab.com/Ma27/cli-helpers/commits/master)

Random collection of some scripts (and completions for it) I use on a daily basis.

## Disclaimer

This repository is basically a fairly opinionated playground of mine which means that some
scripts may behave unintuitively from your perspective and are subject to change. Thus
I recommend to pin this repository to a certain revision and ensure that you understand what's going on.

## Installation

When using [Nix](https://nixos.org/nix) as package manager, all packages and completions can
be installed like this:

```
nix-env -f . -i
```

Optionally a single script can be installed like this:

```
sudo install -Dm755 scripts/script-name.sh /usr/local/bin/script-name
```

Please note that installing `man`-pages is only supported via `Nix` currently.

###### Third-party dependencies

Several scripts need some third-party code which is directly incorporated in this repository.
The details about the licensing are documented in the [License](#License)-section.

While it's automatically taken care of that when installing the package with Nix, you
need to set the environment variable `THIRPARTY_DIR` for the following scripts when
installing those maually:

* `bash_completion/{build-remote,nix-{flame,py,meta}}`
* `scripts/nix-flame`

## Available scripts

The following scripts are available in the `scripts/` directory:

| **Name** | **Dependencies** | **Completion** |
| -------- | ---------------- | -------------- |
| [`borg-nixops`](blob/master/scripts/borg-nixops.sh) | [borg](https://borgbackup.readthedocs.io/en/stable/), [nixops](https://nixos.org/nixops) | no |
| [`clone-fork`](blob/master/scripts/clone-fork.sh) | [git](https://git-scm.com) | no |
| [`git-file`](blob/master/scripts/git-file.sh) | [git](https://git-scm.com/), [vim](https://vim.org) | yes |
| [`nix-meta`](blob/master/scripts/nix-meta.sh) | [Nix](https://nixos.org/nix), [jq](https://stedolan.github.io/jq) | yes |
| [`wifionice-status`](blob/master/scripts/wifionice-status.sh) | [iwgetid](https://linux.die.net/man/8/iwgetid), [jq](https://stedolan.github.io/jq/manual/), [perl](https://www.perl.org/), [curl](https://curl.haxx.se) | yes |
| [`reboot-nixops-machines`](blob/master/scripts/reboot-nixops-machines.sh) | [nixops](https://nixos.org/nixops/), `ssh(1)` | no |
| [`query-nixops-machines`](blob/master/scripts/query-nixops-machines.sh) | [nixops](https://nixos.org/nixops/), [awk](https://www.gnu.org/software/gawk/), [sed](https://www.gnu.org/software/sed/manual/sed.html) | no |
| [`fixup`](blob/master/scripts/fixup) | [git](https://git-scm.com) | no |
| [`todoist-stats`](blob/master/scripts/todoist-stats.py) | [python3](https://www.python.org/), [python-todoist](https://github.com/doist/todoist-python) | yes | 
| [`nix-flame`](blob/master/scripts/nix-flame.sh) | [Nix](https://nixos.org/nix), [flamegraph](https://github.com/brendangregg/FlameGraph) | yes |
| [`nix-py`](blob/master/scripts/nix-py.sh) | [Nix](https://nixos.org/nix) | yes |
| [`build-remote`](blob/master/scripts/build-remote.sh) | [Nix](https://nixos.org/nix) | yes |
| [`nixos-generations`](blob/master/scripts/nixos-generations.sh) | [Nix](https://nixos.org/nix) | no |
| [`read-vcs`](blob/master/scripts/read-vcs.py) | [Python3](https://python.org) | no |


Detailed descriptions and usage examples can be viewed when invoking the script with `--help`.

## Bash completions

For some of these scripts [bash completions](https://opensource.com/article/18/3/creating-bash-completion-script) implemented that can be found in `bash_completions/`.
When installing the repository with [Nix](https://nixos.org/nix), all completions will be deployed into a directory where `bash` finds those scripts.

When using [ZSH](https://www.zsh.org/), the [bashcompinit](https://github.com/zsh-users/zsh/blob/master/Completion/bashcompinit) extension can be
used to activate these completions:

``` zsh
# ~/.zshrc
autoload -U +X bashcompinit && bashcompinit
for i in "$(find ~/.cli-helpers/bash_completions)"; do
  source "$i"
done
```

## `rc/`

The `rc/` directory contains some scripts that are supposed to be sourced by
[bash](https://www.gnu.org/software/bash/) or [zsh](https://zsh.org) and mostly contain
some helpful shell functions and aliases.

For further details, please read the code comments in the files.

## License

This repository is licensed under the [MIT License](https://gitlab.com/Ma27/cli-helpers/blob/master/LICENSE).

Several third-party scripts are incorporated in the `3rdparty/` subtree that were too complicated to get
included during the Nix-based install and are therefore part of the repository. Please note that those are'nt licensed
under MIT.

The following trees are available:

* `3rdparty/nix` (derived from [`NixOS/nix`](https://github.com/NixOS/nix) at `c84c843e3360459cf2c993919fb9f89474a46f38`)
  which is licensed under [LGPL](https://github.com/NixOS/nix/blob/c84c843e3360459cf2c993919fb9f89474a46f38/COPYING).

* `3rdparty/nix-bash-completions` (derived from [`hedning/nix-bash-completions`](https://github.com/hedning/nix-bash-completions) at
  `207d034520766eae9c8d9d0d3473f09d9b4e2fc2`) which is licensed under [BSD3](https://github.com/hedning/nix-bash-completions/blob/207d034520766eae9c8d9d0d3473f09d9b4e2fc2/LICENSE).
