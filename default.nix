{ pkgs ? import <nixpkgs> { }, doCheck ? true }:

let
  pyDeps = ps: [ ps.todoist ps.pytz ps.tzlocal ps.rich ps.icalendar ];
  mypython = pkgs.python3.withPackages pyDeps;
in pkgs.stdenv.mkDerivation rec {
  pname = "cli-helpers";
  version = "${pkgs.lib.fileContents ./version.txt}";
  src = pkgs.lib.cleanSource ./.;

  outputs = [ "out" "man" ];

  # Apart from some linting this is only a script that moves
  # scripts into a store-path -> no need for extra (distributed) building.
  dontBuild = true;
  preferLocalBuild = true;

  buildInputs = [ mypython mypython.pkgs.wrapPython ];

  inherit doCheck;
  checkInputs = [ mypython.pkgs.flake8 pkgs.shellcheck ];
  checkPhase = ''
    export HOME=$(mktemp -d)
    shellcheck scripts/*.sh {rc,bash_completion}/*
    flake8 scripts/*.py
  '';

  nativeBuildInputs = with pkgs; [ makeWrapper docbook5 libxslt libxml2 ];

  postPatch = ''
    patchShebangs scripts/
  '';

  installPhase = ''
    buildPythonPath "${pkgs.lib.concatStringsSep " " (pyDeps mypython.pkgs)}"

    mkdir -p $out/share/ma27-cli-helpers/
    cp -r . $out/share/ma27-cli-helpers
    for i in $(find scripts -type f ! -name "*.swp"); do
      file="''${i%.*}"
      [ "''${i#*.}" = "py" ] && patchPythonScript $out/share/ma27-cli-helpers/$i
      makeWrapper $out/share/ma27-cli-helpers/$i $out/''${file/scripts/bin} \
        --set THIRDPARTY_DIR "$out/share/ma27-cli-helpers/3rdparty"
    done
    for i in $(find bash_completion -type f ! -name "*.swp"); do
      substituteInPlace "$i" \
        --replace '="''${THIRDPARTY_DIR:-"$(pwd)/3rdparty"}"' "=$out/share/ma27-cli-helpers/3rdparty"
      install -Dm644 "$i" "$out/''${i/bash_completion/share/bash-completion/completions}"
    done

    install -Dm644 rc/base.sh $out/libexec/baserc

    mkdir -p $man/share/man
    xmllint --xinclude man/index.xml --output man/index.xml
    xsltproc \
      --param man.output.in.separate.dir 1 \
      --param man.output.base.dir "'$man/share/man/'" \
      ${pkgs.docbook_xsl_ns}/xml/xsl/docbook/manpages/docbook.xsl ./man/index.xml
  '';

  meta.license = with pkgs.lib.licenses; [
    mit               # main sourcetree
    lgpl2Plus         # for nix code in this repo
    bsd3              # for nix-bash-completions (slightly shortened)
  ];
}
