{ pkgs ? import <nixpkgs> { } }:
(import ./. { inherit pkgs; }).overrideAttrs (old: {
  src = null;
  phases = [ "nobuildPhase" ];
  nobuildPhase = ''
    echo "This expression is only used for the development environment!"
    echo "Please use the parent-expression (should be in cli-helpers/default.nix) for building!"
    exit 1
  '';

  buildInputs = old.buildInputs ++ old.checkInputs ++ old.nativeBuildInputs;
})
